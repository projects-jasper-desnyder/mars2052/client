"use strict";

let api;

function loadConfig() {
    fetch("config.json")
        .then(resp => resp.json())
        .then(config => {
            api = `${config.host ? config.host + '/': ''}${config.port ? config.port + '/' : ''}api/`;
            init();
        });
}
